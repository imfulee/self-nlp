import trie

class pa:
    '''
    Class for text processor
    '''
    def __init__(self):
        '''
        initiate a stopword trie and token array
        '''
        self.stopwords = trie.Trie()
        self.tokens = []

    def add_stopword(self, stopword: str):
        '''
        add the stopword
        '''
        self.stopwords.insert(stopword)
    
    def remove_stopwords(self):
        '''
        remove all the stopwords from the tokens
        '''
        # create a shallow copy of the tokens
        # this is to let the for loop to work properly
        tokens_cpy = self.tokens.copy()

        # remove the stopwords from tokens
        for token in tokens_cpy:
            if self.stopwords.exists(token):
                self.tokens.remove(token)    

    def tokenize(self, string: str):
        '''
        tokenize the string and save to tokens<array>
        @param string: the string needed to be tokenized
        '''
        # define delimiters to be replaced
        delimiter = ["\'", "\"", ',', '.', '?', '/']

        # replace the delimiters
        for char in string:
            if char in delimiter:
                string = string.replace(char, ' ')
        
        self.tokens = string.split()

    def lowercase(self):
        '''
        lowercase all the tokens in the list
        '''
        self.tokens = [token.lower() for token in self.tokens]

    def get_tokens(self) -> list:
        '''
        print all the tokens
        '''
        return self.tokens

    def porter_stemmer(self):
        '''
        use the Porter algorithm n tokens
        '''
        # import and initialize the porters algorithm
        from nltk.stem import PorterStemmer
        ps = PorterStemmer()
        
        # stemming the tokens
        self.tokens = [ps.stem(token) for token in self.tokens]

    def remove_duplicate(self):
        '''
        remove any duplicate token
        '''
        self.tokens = list(dict.fromkeys(self.tokens))


if __name__ == "__main__":
    # Read the source file
    with open("src.txt","r") as r_file:
        text = r_file.read().replace('\n', '')
    
    # initialize the class
    data = pa()

    # insert the text and tokenize them
    data.tokenize(text)

    # make the tokens lowercase
    data.lowercase()

    # stemming
    data.porter_stemmer()

    # remove any duplicate tokens
    data.remove_duplicate()

    # Read the stopwords file
    with open("stopwords.txt", 'r') as s_file:
        stopwords = s_file.read().split()

    # adding the stopwords
    for stopword in stopwords:
        stopword = stopword.replace('\n', '')
        data.add_stopword(stopword)
    
    # remove the stopwords
    data.remove_stopwords()
    
    # save the result into output.txt
    with open("result.txt", "w") as w_file:
        w_file.writelines("%s\n" % token for token in data.get_tokens())
