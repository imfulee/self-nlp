import cosine_similarity as cs 
import loading_graphic as lg
import token_proc as tp 
import nb_classifier as nc
import csv

if __name__ == "__main__":
    training_dir = "./training.txt"
    collection_count = 1095
    irtm_dir = "./IRTM/"

    classes = {} # store all the data about training

    '''reading the training file'''
    with open(training_dir, 'r') as training_file:
        for line in training_file:
            line_split = line.split()
            train_list = line_split[1:].copy()
            train_list = [int(train) for train in train_list]
            classes[int(line_split.pop(0))] = train_list.copy()
    training_file.close()

    all_class_index = []
    print("Get all the class index")
    for train_list in classes.values():
        all_class_index += list(set(train_list) - set(all_class_index))

    print("Train the NB")
    vocabulary, prior, cond_prob = nc.train_multinomialnb(
        classes=classes.copy(), collection_count=1095)
    
    index_score = {}
    for doc_index in range(1, collection_count+1):
        lg.loading_graphic(doc_index, collection_count+1, "Scoring")
        if index not in all_class_index:
            test_string = tp.file_to_str(f"{irtm_dir}{doc_index}.txt")
            index, score = nc.apply_multinomialnb(
                classes=classes.copy(), vocabulary=vocabulary.copy(),
                prior=prior.copy(), cond_prob=cond_prob.copy(), 
                document_str=test_string)
            index_score[doc_index] = index
    
    print("Write csv")
    with open('answer.csv', 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["Id", "Value"])
        for doc_index, index in index_score.items():
            writer.writerow(doc_index, index)
    csv_file.close()
            
    
    
