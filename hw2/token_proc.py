'''
Tokenize, remove stopwords and stemming
'''
import trie
from nltk.stem import PorterStemmer

class tokener:
    '''
    Class for tokenizing strings
    '''
    def __init__(self):
        '''
        initiate a stopword trie and token array
        '''
        self.stopwords = trie.Trie()
        self.tokens = []

    def add_stopword(self, stopword: str):
        '''
        add the single stopword
        '''
        self.stopwords.insert(stopword)

    def add_stopwords(self, stopwords: list):
        '''
        add the list of stopwords
        '''
        for stopword in stopwords:
            self.stopwords.insert(stopword)
    
    def remove_stopwords(self):
        '''
        remove all the stopwords from the tokens
        '''
        # create a shallow copy of the tokens
        # this is to let the for loop to work properly
        tokens_cpy = self.tokens.copy()

        # remove the stopwords from tokens
        for token in tokens_cpy:
            if self.stopwords.exists(token):
                self.tokens.remove(token)    

    def tokenize(self, string: str):
        '''
        tokenize the string and save to tokens<array>
        @param string: the string needed to be tokenized
        '''
        # define delimiters to be replaced
        delimiter = ["\'", "\"", ',', '.', '?', '/', '!'
                    ,"(", ")", "-", "`", "_", ";", ":"
                    , "%", "*", "$", "{", "}", "#", "@"]

        number = ["0", "1", "2", "3", '4'
                , '5', '6', '7', '8', '9']

        remove_words = ["&amp"]

        # replace the delimiters
        for char in string:
            if char in delimiter or char in number:
                # remove all the delimiters
                string = string.replace(char, ' ')
        
        self.tokens = string.split()
        self.tokens = [token for token in self.tokens 
                        if token not in remove_words]

    def lowercase(self):
        '''
        lowercase all the tokens in the list
        '''
        self.tokens = [token.lower() for token in self.tokens]

    def get_tokens(self) -> list:
        '''
        return all the tokens
        '''
        return self.tokens

    def porter_stemmer(self):
        '''
        use the Porter algorithm n tokens
        '''
        # import and initialize the porters algorithm
        ps = PorterStemmer()
        
        # stemming the tokens
        self.tokens = [ps.stem(token) for token in self.tokens]

    def remove_duplicate(self):
        '''
        remove any duplicate token
        '''
        self.tokens = list(dict.fromkeys(self.tokens))

def generate_token(string: str) -> list:
    '''
    turn a string into tokens
    @param string: <str> the string that is tokenized
    @return list: the list with all the tokens
    '''

    # initialize the class
    data = tokener()

    # insert the text and tokenize them
    data.tokenize(string)

    # make the tokens lowercase
    data.lowercase()

    # stemming
    data.porter_stemmer()

    # remove any duplicate tokens
    data.remove_duplicate()

    # Read the stopwords file
    with open("stopwords.txt", 'r') as s_file:
        stopwords = s_file.read().split()
            
        # adding the stop words
        data.add_stopwords(stopwords)

        # remove the stopwords
        data.remove_stopwords()
    s_file.close()

    return data.get_tokens()