import re
from collections import OrderedDict, Counter
from nltk.stem.porter import PorterStemmer
import numpy as np
import math
import pandas as pd

'''
get training documents' id for each class
store in training_docs, a 2-D array
'''

file_name = 'training.txt'
training_index_file = open(file_name, 'r')
lines = training_index_file.readlines()
training_docs = []

for line in lines:
    docs = line.split()[1:]
    temp = []
    for doc in docs:
        temp.append(int(doc))
    training_docs.append(temp)


stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves',
             'you', "you're", "you've", "you'll", "you'd", 'your', 'yours',
             'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she',
             "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself',
             'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which',
             'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am',
             'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has',
             'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the',
             'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of',
             'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into',
             'through', 'during', 'before', 'after', 'above', 'below', 'to',
             'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under',
             'again', 'further', 'then', 'once', 'here', 'there', 'when',
             'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few',
             'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not',
             'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't',
             'can', 'will', 'just', 'don', "don't", 'should', "should've",
             'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren',
             "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn',
             "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't",
             'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't",
             'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't",
             'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn',
             "wouldn't"]

extracted_docs = []
dic = dict()
for i in range(len(training_docs)):
    extracted_doc = []
    for j in range(len(training_docs[i])):
        file_name = './IRTM/IRTM/' + str(training_docs[i][j]) + '.txt'
        text_file = open(file_name, 'r')
        lines = text_file.readlines()
        tokens = []

        for line in lines:
            new_arr = re.split('[.?, !;:"`\n\t\'0-9%$()&*#_+\{\}@/]', line)
            new_arr = list(filter(None, new_arr))
            tokens.extend(new_arr)

        tokens = [token.lower() for token in tokens]

        # only strip tokens with '-' at the front
        new_tokens = []
        for token in tokens:
            new_arr = re.split('^-+', token)
            new_arr = list(filter(None, new_arr))
            new_tokens.extend(new_arr)

        tokens = new_tokens

        # user porter's algorithm to stem tokens
        porter_stemmer = PorterStemmer()
        tokens = [porter_stemmer.stem(token) for token in tokens]
        tokens = [token for token in tokens if token not in stopwords]
        tokens = list(dict.fromkeys(tokens))
        extracted_doc.append(tokens)

        # modify document frequency for this document
        for token in tokens:
            if token not in dic:
                dic[token] = 1
            else:
                dic[token] += 1

        text_file.close()
    extracted_docs.append(extracted_doc)

dictionary = OrderedDict(sorted(dic.items(), key=lambda t: t[0]))

# key_arr = term, value_arr = document frequency
vocabulary = list(dictionary.keys())

chi_square_scores = []
for term in vocabulary:
    class_present = []
    class_docs_count = [len(training_docs[i])
                        for i in range(len(training_docs))]
    present_docs_count = [0, 0]
    for i in range(len(training_docs)):
        cnt = 0
        for j in range(len(training_docs[i])):
            if term in extracted_docs[i][j]:
                cnt += 1
                present_docs_count[0] += 1

            else:
                present_docs_count[1] += 1

        class_present.append([cnt, len(training_docs[i]) - cnt])

    chi_square_score = 0
    for i in range(len(training_docs)):
        present_expected = present_docs_count[0] * \
            class_docs_count[i] / sum(present_docs_count)
        absent_expected = present_docs_count[1] * \
            class_docs_count[i] / sum(present_docs_count)
        chi_square_score += (class_present[i][0] -
                             present_expected) ** 2 / present_expected
        chi_square_score += (class_present[i][1] -
                             absent_expected) ** 2 / absent_expected
    chi_square_scores.append(chi_square_score)

indices = np.argsort(chi_square_scores)
indices = indices[-500:]
vocabulary = np.array(vocabulary)
features = vocabulary[indices]
# np.save('features', features)


file_name = 'training.txt'
training_index_file = open(file_name, 'r')
lines = training_index_file.readlines()
training_docs = []

for line in lines:
    docs = line.split()[1:]
    temp = []
    for doc in docs:
        temp.append(int(doc))
    training_docs.append(temp)

# features = np.load('features.npy')
feature_class_prob = []
for feature in features:
    temp = []
    for i in range(len(training_docs)):
        token_count = 0
        occurrence = 0
        for j in range(len(training_docs[i])):
            token_count += len(extracted_docs[i][j])
            occurrence += extracted_docs[i][j].count(feature)
        temp.append((occurrence + 1)/(token_count + len(features)))
    feature_class_prob.append(temp)

# np.save('feature_class_prob', feature_class_prob)


# features = np.load('features.npy')
# feature_class_prob = np.load('feature_class_prob.npy')
file_name = 'training.txt'
training_index_file = open(file_name, 'r')
lines = training_index_file.readlines()
training_docs = []

for line in lines:
    docs = line.split()[1:]
    temp = []
    for doc in docs:
        temp.append(int(doc))
    training_docs.append(temp)
check_training = sum(training_docs, [])

COLLECTIOIN_SIZE = 1095

stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves',
             'you', "you're", "you've", "you'll", "you'd", 'your', 'yours',
             'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she',
             "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself',
             'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which',
             'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am',
             'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has',
             'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the',
             'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of',
             'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into',
             'through', 'during', 'before', 'after', 'above', 'below', 'to',
             'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under',
             'again', 'further', 'then', 'once', 'here', 'there', 'when',
             'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few',
             'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not',
             'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't',
             'can', 'will', 'just', 'don', "don't", 'should', "should've",
             'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren',
             "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn',
             "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't",
             'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't",
             'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't",
             'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn',
             "wouldn't"]

prediction = []
for i in range(COLLECTIOIN_SIZE):
    if (i + 1) in check_training:
        continue
    file_name = './IRTM/IRTM/' + str(i + 1) + '.txt'
    text_file = open(file_name, 'r')
    lines = text_file.readlines()
    tokens = []

    # extract tokens, dump '.', '?', ',', ' ', '!', ';', ':', '"', '`', \n', '\t', numbers, '%',
    # '$', '()', '&', '*', '#', '_', '+', '{}', '@', '/'
    for line in lines:
        new_arr = re.split('[.?, !;:"`\n\t\'0-9%$()&*#_+\{\}@/]', line)
        new_arr = list(filter(None, new_arr))
        tokens.extend(new_arr)

    # lowercasing every token
    tokens = [token.lower() for token in tokens]

    # only strip tokens with '-' at the front
    new_tokens = []
    for token in tokens:
        new_arr = re.split('^-+', token)
        new_arr = list(filter(None, new_arr))
        new_tokens.extend(new_arr)

    tokens = new_tokens

    # user porter's algorithm to stem tokens
    porter_stemmer = PorterStemmer()
    tokens = [porter_stemmer.stem(token) for token in tokens]
    tokens = [token for token in tokens if token not in stopwords]
    tokens = list(dict.fromkeys(tokens))

    classes_score = []
    for j in range(len(training_docs)):
        score = 0
        prior = len(training_docs[j]) / len(check_training)
        score += math.log(prior)
        for token in tokens:
            if token not in features:
                continue
            score += math.log(
                feature_class_prob[np.where(features == token)[0][0]][j])
        classes_score.append(score)

    predicted_class = classes_score.index(max(classes_score)) + 1
    prediction.append([i+1, predicted_class])

# np.save('prediction', prediction)


# prediction = np.load('prediction.npy')

output_format = pd.DataFrame(prediction, columns=['Id', 'Value'])
output_format.to_csv('output.csv', index=False)
