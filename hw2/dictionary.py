'''
Creating a dictionary for term frequency
'''
import math

class dictionary:
    '''
    class that has the term frequencies and inverse document frequency
    '''
    def __init__(self):
        '''
        initialize a term dictionary and inverse document frequency
        '''
        self.tf = {}
        self.idf = {}

    def insert_tokens(self, tokens: list):
        '''
        insert the tokens into the tf<dict>
        @param tokens: <list> the tokens needed to insert
        will insert in dictionary if new token
        otherwise would increment token count
        '''
        for token in tokens:
            if token in self.tf:
                self.tf[token] += 1
            else:
                self.tf[token] = 1

    def get_dict(self) -> dict:
        '''
        returns the dictionary for tf
        '''
        return self.tf

    def sort_dict(self, ascending: bool = False):
        '''
        sort the dictionary
        @param bool: if bool true, then sort by value 
        in ascending order
        '''
        self.tf = {k: v for k, v in sorted(self.tf.items()
                                         , key=lambda item: item[0]
                                         , reverse=ascending)}

    def calc_idf(self, document_number: int):
        '''
        calculate the idf vector
        @param document_number: the number of total documents
        '''
        for key, value in self.tf.items():
            self.idf[key] = math.log(document_number/value)/math.log(10)

    def tf_idf(self, token: str) -> float:
        '''
        calculate the tf-idf for the given token
        @param token: the token that needs to return the tf-idf
        '''
        return self.tf[token] * self.idf[token]
