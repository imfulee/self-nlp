import token_proc as tp

if __name__ == "__main__":
    content = tp.file_to_str("./pa1/pa1.txt")
    token_list = tp.generate_token(content, "./pa1/stopwords.txt")
    print(token_list)