'''
Implemented a trie to speed up the stopwords query
'''
class TrieNode:
    
    def __init__(self, char):
        # the character stored in the node 
        self.char = char

        # whether this node could be the end of a word
        self.is_end = False

        # a dictionary of child nodes
        self.children = {}

class Trie(object):
    def __init__(self):
        '''
        the root node for the trie
        the root does not have a character
        '''
        self.root = TrieNode("")

    def insert(self, word: str):
        '''
        inserting the word lowercased into the tree
        '''
        node = self.root
        
        # lowercase the word
        word = word.lower()

        # for every character in the word
        # check if the character is in children
        for char in word:
            if char in node.children:
                # if character is found then move on
                node = node.children[char]
            else:
                # if character is not found
                # then create a new node and insert into children
                new_node = TrieNode(char)
                node.children[char] = new_node
                node = new_node
        
        # mark the final node/character as end
        node.is_end = True

    def exists(self, word: str) -> bool:
        '''
        check if the word exists in the Trie
        @param word: the word that is queried
        @return boolean: true if the lowercased word is found
        '''
        # get the length of the word
        str_len = len(word)

        # if the string is of length 0, return false
        if str_len == 0:
            return False

        # set root as pointed node
        node = self.root

        # lowercase the word
        word = word.lower()

        # record which character it is on
        char_place = 0

        # search for every character
        # if it is the last character and it is the end of a word
        # return true
        for char in word:
            if char in node.children:
                node = node.children[char]
                char_place = char_place + 1
                if char_place == str_len and node.is_end:
                    return True
            else:
                break
        
        # otherwise return false
        return False