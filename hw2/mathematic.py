'''
all the mathematic functions required
'''
import math
import numpy as np

def unit_vector(vector: list) -> list:
    '''
    returns the unit vector of a 1d list
    '''
    denominator = 0

    for element in vector:
        denominator += element ** 2

    return [element / math.sqrt(denominator) for element in vector]

def cosine_function(vector_a: list, vector_b: list) -> float:
    '''
    returns the inner product of the two unit vectors
    '''
    return np.inner(vector_a, vector_b)
