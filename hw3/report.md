# PA-3

## Tested environment

- Language: Python (version 3.8.6)
- OS: Linux (Pop OS 20.10)

## Usage

Install dependencies by

```bash
pip3 install -r requirements.txt
```

Run the program by

```bash
python3 pa3.py
```

