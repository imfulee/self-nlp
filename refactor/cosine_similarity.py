"""Creating a dictionary for term frequency"""
import math

def sort_dict(dictionary: dict, ascending: bool = False) -> dict:
    """sort dictionary by key

    Args:
        dictionary (dict): The dictionary to be sorted by key
        ascending (bool, optional): If True, then sort by key
            in ascending order. Defaults to False.    
    Returns:
        dict: the sorted dictionary
    """
    return {k: v for k, v in sorted(dictionary.items(), key=lambda item: item[0], reverse=ascending)}

def vector_len(vector: dict) -> int:
    """The length of the vector

    Returns a |v| length

    Args:
        vector (dict): vector

    Returns:
        int: the vector length
    """
    length = 0

    for value in vector.values():
        length += value ** 2
    
    return math.sqrt(length)

def normalize(vector: dict) -> dict:
    """Normalize the vector

    Args:
        vector (dict): the vector that is to be normalized

    Returns:
        dict: the normalized vector
    """
    length = vector_len(vector)
    vector_copy = vector.copy()

    for key, value in vector_copy.items():
        vector_copy[key] = value / length

    return vector_copy

def similarity(vector1: dict, vector2: dict) -> float:
    """Calculate the cosine similarity of the two wf_idf

    Args:
        vector1 (dict): first wf-idf
        vector2 (dict): second wf-idf

    Returns:
        float: the cosine similarity
    """
    # normalize the two vectors
    norm_1 = normalize(vector1)
    norm_2 = normalize(vector2)

    # sort the two vectors
    sorted_1 = sort_dict(norm_1.copy())
    sorted_2 = sort_dict(norm_2.copy())

    # calculate the cosine similarity
    cosine = 0
    for value_1, value_2 in zip(list(sorted_1.values()), list(sorted_2.values())):
        if value_1 == 0 or value_2 == 0:
            continue
        cosine += value_1 * value_2

    return cosine

class df:
    """Stores all the df and idf information"""
    def __init__(self):
        self.df = {}
        self.idf = {}
        self.collection_size = 0

    def insert_tf(self, tf_vector: dict):
        """Insert the df by using tf vector

        Would also increment the collection size count by 1

        Args:
            tf_vector (dict): the tf vector of one document
        """
        for term in tf_vector.keys():
            if term in self.df.keys():
                self.df[term] += 1
            else:
                self.df[term] = 1
        
        self.df = sort_dict(self.df.copy())
        self.collection_size += 1

    def insert_term(self, tokens: list):
        """Insert the df by using a term list of a document

        Would also increment the collection size count by 1

        Args:
            tokens (list): token/term list
        """
        for token in tokens:
            if token in self.df.keys():
                self.df[token] += 1
            else:
                self.df[token] = 1

        self.df = sort_dict(self.df.copy())
        self.collection_size += 1

    def fill_tf(self, tf_vector: dict) -> dict:
        """Provide a term frequency vector with all the terms

        That includes adding terms with frequency = 0

        Args:
            tf_vector (dict): the term frequency vector

        Returns:
            dict: the term frequency vector with missing terms
        """
        tf = tf_vector.copy()

        for term in self.df.keys():
            if tf.get(term) is None:
                tf[term] = 0

        return tf

    def build_idf(self, collection_size: int = -1):
        """calculate the idf vector
        
        Args:
            collection_size (int, optional): the size of the whole collection.
                Defaults to -1, which would indicate the use of the counted
                document size from inserting terms.
        """
        if collection_size < 0:
            collection_size = self.collection_size
        for term, frequency in self.df.items():
            self.idf[term] = math.log10(collection_size / frequency)

    def wf_idf(self, tf_vector: dict) -> dict:
        """Build a vector of wf-idf

        wf is calculated using (1 + log10(frequency_t_d))

        Args:
            tf_vector (dict): The term frequency vector

        Returns:
            dict: wf-idf vector
        """
        wf_idf = {}

        for term, frequency in tf_vector.items():
            if frequency == 0:
                wf_idf[term] = 0
            else:
                wf_idf[term] = (1 + math.log10(frequency)) * self.idf[term]

        return wf_idf

def cosine_sim(tf_vector1: dict, tf_vector2: dict, total_files: int) -> float:
    """Calculate the cosine similarity of two term frequencies

    This acts as a demo function on how to use the above class
    and **SHOULD NOT** be used!

    Args:
        tf_vector1 (dict): tf vector 1
        tf_vector2 (dict): tf vector 2
        total_files (int): the total number of files

    Returns:
        float: cosine similarity
    """
    # initialize dictionary class
    dt = df()

    # add the df
    '''Insert the df into the dictionary
    for some loop:
        dt.insert_tokens(term_list_i)
    '''
    dt.build_idf()
    

    # fill in the frequency = 0 (term, values)
    tf_vector1 = dt.fill_tf(tf_vector1)
    tf_vector2 = dt.fill_tf(tf_vector2)

    wfidf_1 = dt.wf_idf(tf_vector1)
    wfidf_2 = dt.wf_idf(tf_vector2)

    return similarity(wfidf_1, wfidf_2)