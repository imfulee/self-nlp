def loading_graphic(current: int, total: int, optional_text: str=''):
    """generate a loading graphic

    Args:
        current (int): current count
        total (int): total amount of count
        optional_text (str): optional text after word 'Process'
    """
    if current == total:
        print(f"Process: {optional_text}: {current}/{current}")
    else:
        print(f"Process {optional_text}: " 
            + str(current)
            + "/" 
            + str(total), end='\r')