import math
import token_proc as tp
import itertools
from loading_graphic import loading_graphic

stopword_dir = "./stopwords.txt"
irtm_dir = "./IRTM/"

def concat_files_to_str(file_dir: str, file_list: list) -> str:
    """Concatenate the files into one big string

    Args:
        file_dir (str): the folder directory 
        file_list (list): list with all the file names
            excluding the '.txt' end. ex: 1.txt would
            use 1 in the list

    Returns:
        str: the concatenated str
    """
    concatenated_str = ''
    for file_name in file_list:
        concatenated_str += tp.file_to_str(f'{file_dir}{file_name}.txt')
    return concatenated_str

def key_of_max_value(dictionary: dict) -> (str, float):
    """Key of the max value in dictionary

    Args:
        dictionary (dict): searched dictionary
    
    Returns:
        str: key of max value
        float: max value 
    """
    max_key, max_value = '', -math.inf

    for key, value in dictionary.items():
        if value > max_value:
            max_value = value 
            max_key = key
    
    return max_key, max_value

def extract_vocabulary(file_dir: str,
        classes: dict,
        collection_size: int) -> list:
    """Feature selection and build vocabulary

    Args:
        file_dir (str): file directory for building vocabulary
        classes (dict): all the class index and class list
        collection_size (int): size of training data
    Returns:
        list: selected vocabulary
    """
    vocabulary = []
    for i in range(1, collection_size+1):
        string = tp.file_to_str(f"{file_dir}{i}.txt")
        local_vocabulary = tp.generate_token(string, stopword_dir)
        vocabulary += list(set(local_vocabulary) - set(vocabulary))

    term_score = {}
    for term in vocabulary:
        score = compute_score(file_dir, classes, term)
        term_score[term] = score

    return term_score

def sum_count_terms(file_dir: str, file_list: list, duplicate: bool=False) -> int:
    """Total number of terms in the file directory

    Args:
        file_dir (str): file directory
        file_list (list): list of files in the class
        duplicate (bool, optional): remain duplicate or not.
            Defaults to False.

    Returns:
        int: count of terms
    """
    sum_count = 0
    
    for i in file_list:
        string = tp.file_to_str(f"{irtm_dir}{i}.txt")
        sum_count += len(tp.generate_token(string=string,
            stopword_dir=stopword_dir,
            remove_duplicate=duplicate))
        
    return sum_count

def sum_tf(file_dir: str, file_list: list) -> dict:
    """Sum of all tf dictionary

    Args:
        file_dir (str): file directory
        file_list (list): list of files in the class

    Returns:
        dict: summed tf
    """
    sum_tf_dict = {}

    for i in file_list:
        string = tp.file_to_str(f"{file_dir}{i}.txt")
        tf_dict = tp.generate_tf(string, stopword_dir)
        
        for term, frequency in tf_dict.values():
            if sum_tf_dict.get(term) is None:
                sum_tf_dict[term] = frequency
            else:
                sum_tf_dict[term] += frequency
    
    return sum_tf_dict
        

def training_nb(classes: dict, file_dir: str, training_size: int):
    print("Process: Extract vocabulary")
    vocabulary = extract_vocabulary()
    number_of_docs = training_size # number of documents
    
    prior, cond_prob = {}, {}
    # calculating prior probabilities
    for class_index, class_list in classes.items():
        loading_graphic(class_index, len(class_index), 
            "Training index")
        prior[class_index] = len(class_list) / number_of_docs
        
        # concat the text of a class into a string
        concat_text = concat_files_to_str(file_dir, class_list)
        # generate tokens from concat text
        concat_token = tp.generate_token(string=concat_text, 
            stopword_dir=stopword_dir, remove_duplicate=False)

        # build the count tokens of term
        term_ct = {}
        for term in vocabulary:
            term_ct[term] = concat_token.count(term)

        # build conditonal probability
        sum_token_of_class = sum_count_terms(irtm_dir, class_list)
        vocab_len_of_class = len(sum_tf(irtm_dir, class_list))
        for term in vocabulary:
            cond_prob[(term, class_index)] = (term_ct[term] + 1) / (
                sum_token_of_class + vocab_len_of_class)

    return vocabulary, prior, cond_prob
        


            

    


def count_tokens_of_term(text: str, term: str) -> int:
    """Count the number of tokens of a term in the text

    Args:
        text (str): the text to be counted
        term (str): term that we are counting

    Returns:
        int: number of terms in text
    """
    term_frequency = tp.generate_tf(text, stopword_dir=stopword_dir)
    if term_frequency.get(term) is None:
        return 0
    return term_frequency[term]

def train_multinomialnb(classes: dict, collection_count: int, file_dir: str=irtm_dir) -> (list, dict, dict):
    """Train the multinomial naives bayes classifier

    Args:
        classes (dict): all classes with their training document
        collection_count (int): the amount of collection
        file_dir (str, optional): file directory for all documents. 
            Defaults to irtm file.

    Returns:
        list: vocabulary list 
        dict: prior probability of term
        dict: conditional probability
    """
    
    vocabulary = [] # vocabulary list
    # get all the training document index
    training_index = []
    for class_list in classes.values():
        training_index += class_list
    
    # build the dictionary
    for i in training_index:
        doc_string = tp.file_to_str(f"{file_dir}{i}.txt")
        tokens = tp.generate_token(doc_string, stopword_dir=stopword_dir)
        vocabulary += list(set(tokens) - set(vocabulary))

    n_c = {} # index, N_c: store the amount of document in a class 
    cond_prob = {} # conditional probability of term

    # count the total amount of training documents
    train_doc_count = 0 # amount of documents in training set
    for class_list in classes.values():
        train_doc_count += len(class_list)

    prior = {} # store all prior probability p(c)
    for class_index, class_list in classes.items():
        # build the prior probability
        n_c[class_index] = len(class_list)
        prior[class_index] = n_c[class_index] / train_doc_count

        # concat all the files in the class into a str
        concated_str = concat_files_to_str(file_dir=irtm_dir, file_list=class_list)
        t_ct = {} # token_count_of_term
        for term in vocabulary:
            t_ct[term] = count_tokens_of_term(concated_str, term=term)

        # conditional probability
        for term in vocabulary:
            class_prob = {}
            class_prob[class_index] = (t_ct[term] + 1) / (sum(t_ct.values()) + len(t_ct))
            if cond_prob.get(term) is None:
                cond_prob[term] = class_prob.copy()
            else:
                cond_prob[term][class_index] = class_prob[class_index]

    return vocabulary, prior, cond_prob

def apply_multinomialnb(classes: dict, vocabulary: list, prior: dict, cond_prob: dict, document_str: str) -> (str, float):
    """Calculate score for naive bayes classification

    Args:
        classes (dict): all classes with their training document index 
        vocabulary (list): vocabulary list
        prior (dict): prior probability of term
        cond_prob (dict): conditional probability
        document_str (str): string to be applied on

    Returns:
        str: category of document
        float: the naive bayes score
    """
    term_list = tp.generate_token(document_str)
    term_list = list(set(term_list) - (set(term_list) - set(vocabulary)))  
    
    score = {}
    for class_index in classes.keys():
        score[class_index] = math.log10(prior[class_index])
        for term in term_list:
            score[class_index] += math.log10(cond_prob[term][class_index])

    return key_of_max_value(score)





        