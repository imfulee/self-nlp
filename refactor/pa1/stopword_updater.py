import nltk
nltk.download('stopwords')

from nltk.corpus import stopwords

stopword_list = stopwords.words('english')
with open ('stopwords.txt', 'w') as w_file:
    for stopword in stopword_list:
        w_file.write("%s\n" % stopword)
w_file.close()