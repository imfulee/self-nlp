# PA-1

## Environment

- Language: Python, version 3.8.2
- Tested with Bash on PopOS (Ubuntu derivative)

## Execution

To install the required pip files

```bash
pip3 install -r requirements.txt
```

To run the program

```bash
python3 pa1.py
```

it should output a txt file, called `result.txt`