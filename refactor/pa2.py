import dictionary as dc
import token_proc as tp
import mathematic as mt
from loading_graphic import loading_graphic
import pdb
        
if __name__ == "__main__":
    # define the amount of files to go through
    FOLDERSIZE = 1095
    stopword_dir = "./pa2/stopwords.txt"
    dictionary = dc.dictionary(FOLDERSIZE)

    for i in range(1, FOLDERSIZE+1):
        loading_graphic(i, FOLDERSIZE, "Generate token from file")
        
        # generate tokens from a file
        contents = tp.file_to_str(f"./pa2/IRTM/{i}.txt")
        token_list = tp.generate_token(contents, stopword_dir=stopword_dir)
        
        # place token in the dictionary
        dictionary.insert_tokens(token_list)

    # write the dictionary into a file
    print("Process: Write document frequency to text")
    dictionary.df_write_text(filename="./pa2/df.txt")
    '''try make this for individual files'''
    # Build term frequency
    for i in range(1, FOLDERSIZE+1):
        loading_graphic(i, FOLDERSIZE, "Generate term frequencies from file")

        contents = tp.file_to_str(f"./pa2/IRTM/{i}.txt")
        tp_i = tp.generate_tf(contents, stopword_dir=stopword_dir)
        dictionary.insert_tf(document_number=i, term_frequency=tp_i)
    
    print("Process: Calculate inverse document frequency")
    dictionary.calc_idf()

    print("Process: Calculate tf-idf")
    dictionary.sublinear_tf()
    dictionary.calc_tfidf(doc_number=1, options="sublinear")
    dictionary.calc_tfidf(doc_number=2, options="sublinear")
    
    print("Process: Calculate cosine similarity")
    print("Cosine =", dictionary.cosine_similarity(doc_1=1, doc_2=2, options='sublinear'))


    