"""Creating a dictionary for term frequency"""
import math

def sort_dict(dictionary: dict, ascending: bool = False) -> dict:
    """sort dictionary by key

    Args:
        dictionary (dict): The dictionary to be sorted by key
        ascending (bool, optional): If True, then sort by key
            in ascending order. Defaults to False.    
    Returns:
        dict: the sorted dictionary
    """
    return {k: v for k, v in sorted(dictionary.items(), key=lambda item: item[0], reverse=ascending)}

def vector_len(vector: dict) -> int:
    """The length of the vector

    Returns a |v| length

    Args:
        vector (dict): vector

    Returns:
        int: the vector length
    """
    length = 0

    for value in vector.values():
        length += value ** 2
    
    return math.sqrt(length)

def normalize(vector: dict) -> dict:
    """Normalize the vector

    Args:
        vector (dict): the vector that is to be normalized

    Returns:
        dict: the normalized vector
    """
    length = vector_len(vector)
    vector_copy = vector.copy()

    for key, value in vector_copy.items():
        vector_copy[key] = value / length

    return vector_copy

def similarity(vector1: dict, vector2: dict) -> float:
    """Calculate the cosine similarity of the two wf_idf

    Args:
        vector1 (dict): first wf-idf
        vector2 (dict): second wf-idf

    Returns:
        float: the cosine similarity
    """
    # normalize the two vectors
    norm_1 = normalize(vector1)
    norm_2 = normalize(vector2)

    # sort the two vectors
    sorted_1 = sort_dict(norm_1.copy())
    sorted_2 = sort_dict(norm_2.copy())

    # calculate the cosine similarity
    cosine = 0
    for value_1, value_2 in zip(list(sorted_1.values()), list(sorted_2.values())):
        if value_1 == 0 or value_2 == 0:
            continue
        cosine += value_1 * value_2

    return cosine

class dictionary:
    """class that has the term frequencies and inverse document frequency"""

    def __init__(self, document_size: int):
        """initialize a term dictionary, document frequency and inverse document frequency

        Args:
            document_size (int): the amount of documents counted
        """
        self.tf = {}
        self.df = {}
        self.idf = {}
        self.tf_idf = {}
        self.document_size = document_size

    def insert_tokens(self, tokens: list):
        """Insert the tokens into the tf<dict>

        - Will insert in dictionary if new token
        - Otherwise, would increment token count

        Args:
            tokens (list): the tokens needed to insert
        """
        for token in tokens:
            if token in self.df:
                self.df[token] += 1
            else:
                self.df[token] = 1

        self.df = sort_dict(self.df)

    def insert_tf(self, document_number: int, term_frequency: dict):
        """Inserting the term frequency for each document for multiple documents

        Args:
            document_number (int): the number of the document
            term_frequency (dict): the term frequency dictionary
        """
        for term in self.df.keys():
            # adding the missing terms into the tf's in various documents
            if term_frequency.get(term) is None:
                # if there are no doc_term in the tf dictionary
                # add the key into the tf dict and value = 0
                term_frequency[term] = 0

        self.tf[document_number] = term_frequency

    def fill_tf(self, term_frequency: dict) -> dict:
        """Provide a term frequency vector with all the terms

        That includes adding terms with frequency = 0

        Args:
            term_frequency (dict): the term frequency

        Returns:
            dict: the term frequency vector with missing terms
        """
        tf = term_frequency.copy()

        for term in self.df.keys():
            if tf.get(term) is None:
                tf[term] = 0

        return tf

    def sublinear_tf(self):
        """Generate a sublinear tf function

        Declare a sublinear tf dictionary in the class.
        Create a term with value 0 inside the term frequency if not present.
        The sublinear function would take all the tf and 1 + log(tf) it.
        """
        self.subl_tf = {}

        for doc_number, tf_i in self.tf.items():
            subl_tf = {}

            # build the sublinear term frequency for a single document
            for term, frequency in tf_i.items():
                if frequency == 0:
                    subl_tf[term] = 0
                else:
                    subl_tf[term] = 1 + math.log10(frequency)

            # assign the sublinear term frequency to the document number
            self.subl_tf[doc_number] = subl_tf.copy()

    def wf_idf(self, term_frequency: dict) -> dict:
        """Build a vector of wf-idf

        Args:
            term_frequency (dict): The term frequency vector

        Returns:
            dict: wf-idf vector
        """
        wf_idf = {}

        for term, frequency in term_frequency.items():
            if frequency == 0:
                wf_idf[term] = 0
            else:
                wf_idf[term] = (1 + math.log10(frequency)) * self.idf[term]

        return wf_idf

    def calc_idf(self):
        """calculate the idf vector"""
        for key, value in self.df.items():
            self.idf[key] = math.log10(self.document_size/value)

    def all_tfidf(self, options: str = 'sublinear'):
        """Generate the tf-idf for all documents

        Would save it to a variable called self.tf_idf regardless of options

        Args:
            options (str, optional): if 'tf' then tf-idf 
                would be calculated. If 'sublinear', then
                would calculate wf-idf.
        """
        if options == 'tf':
            for doc_number in range(1, self.document_size + 1):
                tf_idf = {}
                for term, frequency in self.tf[doc_number].items():
                    tf_idf[term] = frequency * self.idf[term]
                self.tf_idf[doc_number] = tf_idf.copy()

        if options == 'sublinear':
            for doc_number in range(1, self.document_size + 1):
                wf_idf = {}
                for term, frequency in self.subl_tf[doc_number].items():
                    wf_idf[term] = frequency * self.idf[term]
                self.tf_idf[doc_number] = wf_idf.copy()

    def calc_tfidf(self, doc_number: int, options: str='sublinear'):
        """Generate specific tf-idf for document number

        Args:
            doc_number (int): the document which to 
                calculate tf-idf
            options (str, optional): use 'tf' mode
                or 'sublinear' mode. Defaults to 'sublinear'.
        """
        if options == 'tf':
            term_frequency = self.tf[doc_number].copy()
        elif options == 'sublinear':
            term_frequency = self.subl_tf[doc_number].copy()
        
        tf_idf = {}
        for term, frequency in term_frequency.items():
            tf_idf[term] = frequency * self.idf[term]
        
        self.tf_idf[doc_number] = tf_idf

    def df_write_text(self, filename: str):
        """Write the dictionary into a txt file

        The write format would be as follow:
        index <space> term <space> frequency

        Args:
            filename (str): the filename to be written to
        """
        with open(f"{filename}", 'w') as w_file:
            for index, df_i in enumerate(self.df):
                w_file.write(f"{index+1} {df_i} {self.df[df_i]}\n")
        w_file.close()

    def vector_length(self, doc_number: int) -> int:
        """Calculate the vector length for tf-idf of a document

        Args:
            doc_number (int): the document number

        Returns:
            int: vector length
        """
        length = 0
        vector = self.tf_idf[doc_number].copy()

        for frequency in vector.values():
            if frequency != 0:
                length += frequency ** 2

        return math.sqrt(length)

    def norm_tfidf(self, doc_number: int) -> dict:
        """calculate the normalized tf-idf

        Args:
            doc_number (int): document number

        Returns:
            dict: normalized tf-idf
        """
        vector_len = self.vector_length(doc_number)

        tfidf_dict = self.tf_idf[doc_number].copy()
        
        for term, tfidf in tfidf_dict.items():
            if tfidf != 0:
                tfidf_dict[term] = tfidf / vector_len

        return tfidf_dict


    def cosine_similarity(self, doc_1: int, doc_2: int, options: str='sublinear') -> float:
        """Calculate the cosine similarity for the two documents

        Args:
            doc_1 (int): document number for document 1
            doc_2 (int): document number for document 2
            options (str, optional): the mode for calculating either 'tf' or 'sublinear'. Default to 'sublinear'

        Returns:
            float: cosine similarity
        """
        cosine = 0

        self.tf_idf[doc_1] = sort_dict(self.tf_idf[doc_1].copy())
        self.tf_idf[doc_2] = sort_dict(self.tf_idf[doc_2].copy())

        for value_1, value_2 in zip(list(self.norm_tfidf(doc_1).values()), list(self.norm_tfidf(doc_2).values())):
            if value_1 == 0 or value_2 == 0:
                continue
            cosine += value_1 * value_2
        
        return cosine


def cosine_sim(tf_vector1: dict, tf_vector2: dict, total_files: int) -> float:
    """Calculate the cosine similarity of two term frequencies

    This acts as a demo function on how to use the above class
    and **SHOULD NOT** be used!

    Args:
        tf_vector1 (dict): tf vector 1
        tf_vector2 (dict): tf vector 2
        total_files (int): the total number of files

    Returns:
        float: cosine similarity
    """
    # initialize dictionary class
    dt = dictionary(total_files)

    # add the df
    '''Insert the df into the dictionary
    for some loop:
        dt.insert_tokens(term_list_i)
    dt.calc_idf()
    '''

    # fill in the frequency = 0 (term, values)
    tf_vector1 = dt.fill_tf(tf_vector1)
    tf_vector2 = dt.fill_tf(tf_vector2)

    wfidf_1 = dt.wf_idf(tf_vector1)
    wfidf_2 = dt.wf_idf(tf_vector2)

    return similarity(wfidf_1, wfidf_2)
