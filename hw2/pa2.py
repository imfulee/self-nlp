import dictionary as dc
import token_proc as tp
import mathematic as mt
        
if __name__ == "__main__":
    '''
    Generate the dictionary
    '''

    # for every file there is
    total_file_number = 1095
    
    # initialize the dictionary class
    term_freq = dc.dictionary()
    
    for document_number in range(1, total_file_number + 1):
        # progress bar
        print(f"File %4d/{total_file_number}" % document_number, end='\r')

        # open the selected file and read it
        with open(f"IRTM/{document_number}.txt", 'r') as r_file:
            # turn the file into a string
            text = r_file.read().replace('\n', ' ')

            # generate a token list
            token_list = tp.generate_token(text)

            # add token list into dictionary
            term_freq.insert_tokens(token_list)
        r_file.close()

    # sort the dictionary
    term_freq.sort_dict()
    
    index = 1
    with open("dictionary.txt", 'w') as w_file:
        # write the dictionary to file
        for token, number in term_freq.get_dict().items():
            w_file.writelines(str(index) + " " 
                            + token + " " 
                            + str(number) + '\n')
            index += 1
    w_file.close()

    print("[Done] Dictionary generated")

    '''
    create the tf_idf unit vector for documents
    '''
    # calculate the idf for every term
    term_freq.calc_idf(document_number)

    # generate the tf-idf unit vector for all documents
    for document_number in range(1, total_file_number + 1):
        # variables for the generation of tf-idf unit-vector
        index, tf_idf_dict = 1, {}
        
        # progress bar
        print(f"File %4d/{total_file_number}" % document_number, end='\r')


        # open the selected file and read it
        with open(f"IRTM/{document_number}.txt", 'r') as r_file:
            # turn the file into a string
            text = r_file.read().replace('\n', ' ')
        r_file.close()

        # generate a token list
        token_list = tp.generate_token(text)

        # for every term term in the dictionary 
        for token, value in term_freq.get_dict().items():
            # if the token is in the token list
            if token in token_list:
                # place the tf_idf values into a dictionary
                tf_idf_dict[index] = term_freq.tf_idf(token)
            index += 1

        # calculate the unit vector list 
        unit_vector_list = mt.unit_vector(list(tf_idf_dict.values()))

        # zip the unit vector with the indexes
        unit_vector = dict(zip(list(tf_idf_dict.keys()), unit_vector_list))

        # open up a number file and write the 
        with open(f"TFIDF/{document_number}.txt", 'w') as w_file:        
            # write into the files the unit vector
            for key, value in unit_vector.items():
                w_file.writelines(str(key) + " " 
                                + str(value) + '\n')
                                
        w_file.close()

    print("[Done] TF-IDF unit vector generated")
    
    '''
    generate the cosine functions
    '''

    # vector 1 and 2 store the unit vector 
    # for document 1 and 2
    vector_1, vector_2 = {}, {}
    with open("TFIDF/1.txt", 'r') as r_file:
        for line in r_file:
            # tlist is a temporary list
            tlist = line.split()
            # add the line into a dictionary
            vector_1[int(tlist[0])] = float(tlist[1])
    r_file.close()

    with open("TFIDF/2.txt", 'r') as r_file:
        for line in r_file:
            # tlist is a temporary list
            tlist = line.split()
            # add the line into a dictionary
            vector_2[int(tlist[0])] = float(tlist[1])
    r_file.close()

    # get the dictionary size
    with open("dictionary.txt", 'r') as r_file:
        dict_len = len(list(r_file))
    r_file.close()

    # fill up the zeros and sort
    for index in range(1, dict_len + 1):
        if index not in vector_1:
            vector_1[index] = 0
        if index not in vector_2:
            vector_2[index] = 0

    vector_1, vector_2 = sorted(vector_1.items()), sorted(vector_2.items())
    list_1 = [item[1] for item in vector_1]
    list_2 = [item[1] for item in vector_2]
    print(list_2)
    print("cosine = ", mt.cosine_function(list_1, list_2))