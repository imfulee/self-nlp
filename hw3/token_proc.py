"""Tokenize, remove stopwords and stemming"""
import trie
from nltk.stem import PorterStemmer

class tokener:
    """Class for tokenizing strings"""
    def __init__(self):
        """initiate a stopword trie and token array"""
        self.stopwords = trie.Trie()
        self.tokens = []
        self.term_frequency = {}

    def add_stopword(self, stopword: str):
        """add the single stopword"""
        self.stopwords.insert(stopword)

    def add_stopwords(self, stopwords:list):
        """add the stopword list

        Args:
            stopwords (list): the list of stopwords
        """
        for stopword in stopwords:
            self.stopwords.insert(stopword)
    
    def remove_stopwords(self):
        """remove all the stopwords from the tokens"""
        # create a shallow copy of the tokens
        # this is to let the for loop to work properly
        tokens_cpy = self.tokens.copy()

        # remove the stopwords from tokens
        for token in tokens_cpy:
            if self.stopwords.exists(token):
                self.tokens.remove(token)    

    def tokenize(self, string: str, lowercase: bool=True):
        """Tokenize the string and save to tokens<array>

        Args:
            string (str): the string needed to be tokenized
            lowercase (bool, optional): lowercase the string. Defaults to True
        """
        # define delimiters to be replaced
        delimiter = ["\'", "\"", ',', '.', '?', '/', '!'
                    ,"(", ")", "-", "`", "_", ";", ":"
                    , "%", "*", "$", "{", "}", "#", "@"]

        number = ["0", "1", "2", "3", '4'
                , '5', '6', '7', '8', '9']
        
        if lowercase:
            string = string.lower()

        # replace the delimiters
        for char in string:
            if char in delimiter or char in number:
                # remove all the delimiters
                string = string.replace(char, ' ')
        
        self.tokens = string.split().copy()

    def lowercase_list(self):
        """lowercase all the tokens in the list"""
        self.tokens = [token.lower() for token in self.tokens]

    def get_tokens(self) -> list:
        """return all the tokens in a list"""
        return self.tokens.copy()

    def porter_stemmer(self):
        """use the Porter algorithm on tokens"""
        # import and initialize the porters algorithm
        ps = PorterStemmer()
        
        # stemming the tokens
        self.tokens = [ps.stem(token) for token in self.tokens]
        self.tokens = [token for token in self.tokens
                             if token != "&amp"]

    def build_tf(self):
        """Build the term frequency for the document"""
        for token in self.tokens:
            if self.term_frequency.get(token) is None:
                self.term_frequency[token] = 1
            else:
                self.term_frequency[token] += 1

    def get_tf(self) -> dict:
        """Return the term frequency

        Returns:
            dict: the term frequency dictionary
        """
        return self.term_frequency.copy()

    def remove_duplicate(self):
        """remove any duplicate token"""
        self.tokens = list(dict.fromkeys(self.tokens))

def generate_token(string: str, stopword_dir: str = '', remove_duplicate: bool=True) -> list:
    """Turn a string into tokens and creates a stopword list from directory

    Args:
        string (str): the string that is tokenized
        stopword_dir (str, optional): the string that specifies the stopword file directory. Defaults to ''
        remove_duplicate (bool, optional): remove duplicate if true
    Returns:
        list: the list with all the tokens
    """
    # initialize the class
    data = tokener()

    # insert the text and tokenize them
    data.tokenize(string, lowercase=True)

    # stemming
    data.porter_stemmer()

    if stopword_dir != '':
        # Read the stopwords file
        with open(f"{stopword_dir}", 'r') as s_file:
            stopwords = s_file.read().split()
                
            # adding the stop words
            data.add_stopwords(stopwords)

            # remove the stopwords
            data.remove_stopwords()
        s_file.close()

    if remove_duplicate:
        # remove any duplicate tokens
        data.remove_duplicate()

    return data.get_tokens()

def generate_tf(string: str, stopword_dir: str = '') -> dict:
    """Generate a term frequency dictionary

    Args:
        string (str): the string that is tokenized
        stopwird_dir (str, optional): the string that specifies the stopword file directory. Defaults to ''

    Returns:
        dict: the list with all the term frequencies
    """
    # initialize the class
    data = tokener()

    # insert the text and tokenize them
    data.tokenize(string, lowercase=True)

    # stemming
    data.porter_stemmer()

    if stopword_dir != '':
        # Read the stopwords file
        with open(f"{stopword_dir}", 'r') as s_file:
            stopwords = s_file.read().split()
                
            # adding the stop words
            data.add_stopwords(stopwords)

            # remove the stopwords
            data.remove_stopwords()
        s_file.close()

    data.build_tf()

    return data.get_tf()



def file_to_str(file_directory: str) -> str:
    """returns a string that has the contents of the file without '\\n'

    Args:
        file_directory (str): the file needed to be read

    Returns:
        str: returned string
    """
    with open(f"{file_directory}", "r") as r_file:
        return r_file.read().replace('\n', ' ')
    r_file.close()
